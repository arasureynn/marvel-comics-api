namespace MarvelSharp.Models.Characters
{
	public class CharacterDataWrapper
	{
		public int Code { get; set; }
		public string Status { get; set; }
		public CharacterDataContainer Data { get; set; }
		public string Etag { get; set; }


		public CharacterDataWrapper ()
		{
		}
	}
}

