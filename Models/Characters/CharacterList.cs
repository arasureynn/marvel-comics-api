﻿namespace MarvelSharp.Models.Characters
{
    public class CharacterList
    {
        public int Available { get; set; }
        public int Returned { get; set; }
        public dynamic CollectionURI { get; set; }
        public CharacterSummary[] Items { get; set; }

        public CharacterList()
        {
            
        }
    }
}
