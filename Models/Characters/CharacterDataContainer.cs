﻿namespace MarvelSharp.Models.Characters
{
    public class CharacterDataContainer
    {
        public int Offset { get; set; }
        public int Limit { get; set; }
        public int Total { get; set; }
        public int Count { get; set; }
        public Character[] Results { get; set; }

        public CharacterDataContainer()
        {
        }
    }
}
