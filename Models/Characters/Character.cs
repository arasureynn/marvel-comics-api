using System;
using System.Collections.Generic;
using System.Linq;
using MarvelSharp.Models.Comics;
using MarvelSharp.Models.Events;
using MarvelSharp.Models.Series;
using MarvelSharp.Models.Stories;
using Newtonsoft.Json;
using Xunit;

namespace MarvelSharp.Models.Characters
{
    public class Character
    {
        #region properties

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Modified { get; set; }
        public string ResourceUri { get; set; }
        public Url[] Urls { get; set; }
        public Image Thumbnail { get; set; }
        public ComicList Comics { get; set; }
        public StoryList Stories { get; set; }
        public EventList Events { get; set; }
        public SeriesList Series { get; set; }

        #endregion

        #region constructor

        public Character()
        {
        }

        public Character(int id)
        {
            this.Id = id;
        }

        #endregion

        #region methods

        /// <summary>
        /// Get a character by ID, this will probably be used mostly if we have a series that just provides a character ID, or possibly never
        /// </summary>
        /// <param name="characterId">Character ID we want to look up.</param>
        /// <returns></returns>
        public static Character GetCharacterById(int characterId)
        {
            return Utilities.GetJObject<Character>("characters/{id}", characterId);
        }

        /// <summary>
        /// Search the database for a specific character
        /// </summary>
        /// <param name="characterName">The character you are searching for.</param>
        /// <returns></returns>
        public static Character GetCharacterByName(string characterName)
        {
            var parameters = new Dictionary<string, string> {{"name", characterName}};

            return Utilities.GetJObject<Character>("characters", parameters);
        }

        /// <summary>
        /// Returns a list of characters
        /// </summary>
        /// <param name="limit">Limit per api query, has to be 100 or below, will return 409 error if above</param>
        /// <param name="offset">start of the query, default 0 to start at the beginning of the results, if offset is 100 and limit is 100 then you will be getting results 100-200</param>
        /// <param name="getAll">Whether to loop and get all possible results or just the first from limit set.</param>
        /// <returns></returns>
        public static List<Character> GetCharacters(int limit = 100, int offset = 0, bool getAll = false)
        {
            var array = Utilities.GetJArray("characters", limit, offset, getAll);

            // Convert json data to Character object, add to Character list to be returned later
            return new List<Character>(array.Select(token => JsonConvert.DeserializeObject<Character>(token.ToString())));
        }

        public static List<Character> CharactersGetByPartialName(string charName = null, int limit = 100, bool getAll = true)
        {
            if (!string.IsNullOrEmpty(charName))
            {
                var parameters = new Dictionary<string, string>
                {
                    {"nameStartsWith", charName},
                };
                var array = Utilities.GetJArray("characters", parameters: parameters);
                return new List<Character>(array.Select(token => JsonConvert.DeserializeObject<Character>(token.ToString())));
            }
            throw new ArgumentException("Please provide an ID to search for");
        } 

        public List<Comic> GetComicsByCharacter(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("characters/{id}/comics", limit, offset, getAll, parameters);
            return new List<Comic>(array.Select(t=>JsonConvert.DeserializeObject<Comic>(t.ToString())));
        }

        public List<Event> GetEventsByCharacter(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("characters/{id}/events", limit, offset, getAll, parameters);
            return new List<Event>(array.Select(t => JsonConvert.DeserializeObject<Event>(t.ToString())));
        }

        public List<Series.Series> GetSeriesByCharacter(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("characters/{id}/series", limit, offset, getAll, parameters);
            return new List<Series.Series>(array.Select(t => JsonConvert.DeserializeObject<Series.Series>(t.ToString())));
        }

        public List<Story> GetStoriesByCharacter(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { { "id", Id.ToString() } };
            var array = Utilities.GetJArray("characters/{id}/stories", limit, offset, getAll, parameters);
            return new List<Story>(array.Select(t => JsonConvert.DeserializeObject<Story>(t.ToString())));
        }
        
        #endregion

        #region tests

        [Fact]
        public void CharacterTestGetAllComics()
        {
            List<Character> characters = GetCharacters(20);
            Console.WriteLine("Character Count: {0}", characters.Count);
            Assert.True(characters.Count == 20, "Failed to get 100 characters.");
        }

        [Fact]
        public void CharacterGetByPartialName()
        {
            List<Character> characters = CharactersGetByPartialName("Spider");
            foreach (Character character in characters)
            {
                Console.WriteLine(character.Name);
            }
            Assert.True(characters.Count > 0, "Failed to get any characters starting with Spider");
        }

        [Fact]
        public void CharacterTestGetCharacterById()
        {
            const string characterName = "Wolverine";

            Character c = GetCharacterByName(characterName);

            Character c2 = GetCharacterById(c.Id);

            Assert.True(c2.Name.Equals(c.Name), "Characters do not match");
        }

        [Fact]
        public void CharacterTestGetCharacterByName()
        {
            const string characterName = "Wolverine";

            Character c = GetCharacterByName(characterName);

            Assert.True(c.Name.Equals(characterName), "Character name doesn't match, ");
        }

        // using character ID 1009610 for Spider-Man
        [Fact]
        public void CharacterTestGetSeriesByCharacter()
        {
            Character cha = new Character(1009610);
            List<Series.Series> series = cha.GetSeriesByCharacter(50);
            foreach (Series.Series se in series)
            {
                Console.WriteLine(se.Title);
            }
            Assert.True(series.Count > 0 && series.Count <= 50, "Failed to get a list of series for Spider-Man");
        }

        [Fact]
        public void CharacterTestGetEventsByCharacter()
        {
            Character cha = new Character(1009610);
            List<Event> events = cha.GetEventsByCharacter(50);
            foreach (Event se in events)
            {
                Console.WriteLine(se.Title);
            }
            Assert.True(events.Count > 0 && events.Count <= 50, "Failed to get a list of events for Spider-Man");
        }

        [Fact]
        public void CharacterTestGetStoriesByCharacter()
        {
            Character cha = new Character(1009610);
            List<Story> stories = cha.GetStoriesByCharacter(50);
            foreach (Story se in stories)
            {
                Console.WriteLine(se.Title);
            }
            Assert.True(stories.Count > 0 && stories.Count <= 50, "Failed to get a list of stories for Spider-Man");
        }

        [Fact]
        public void CharacterTestGetComicsByCharacter()
        {
            Character cha = new Character(1009610);
            List<Comic> comics = cha.GetComicsByCharacter(50);
            foreach (Comic se in comics)
            {
                Console.WriteLine(se.Title);
            }
            Assert.True(comics.Count > 0 && comics.Count <= 50, "Failed to get a list of comics for Spider-Man");
        }

        #endregion
    }

    public class Url
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("url")]
        public string URL { get; set; }
    }

    public class Image
    {
        public string Path { get; set; }
        public string Extension { get; set; }
    }
}