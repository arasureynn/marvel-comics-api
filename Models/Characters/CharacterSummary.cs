﻿namespace MarvelSharp.Models.Characters
{
    public class CharacterSummary
    {
        public string ResourceURI { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }

        public CharacterSummary()
        {
            
        }
    }
}
