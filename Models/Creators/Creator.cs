﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Policy;
using MarvelSharp.Models.Characters;
using MarvelSharp.Models.Comics;
using MarvelSharp.Models.Events;
using MarvelSharp.Models.Series;
using MarvelSharp.Models.Stories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;
using Url = MarvelSharp.Models.Characters.Url;

namespace MarvelSharp.Models.Creators
{
    public class Creator
    {
        #region properties

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string FullName { get; set; }
        public string Modified { get; set; }
        public string ResourceURI { get; set; }
        public Url[] Urls { get; set; }
        public Image Thumbnail { get; set; }
        public SeriesList Series { get; set; }
        public StoryList Stories { get; set; }
        public ComicList Comics { get; set; }
        public EventList Events { get; set; }

        #region ENUMS

        public enum OrderBy
        {
            [Description("lastName")] LastNameAsc,
            [Description("firstName")] FirstNameAsc,
            [Description("middleName")] MiddleNameAsc,
            [Description("suffix")] SuffixAsc,
            [Description("modified")] ModifiedAsc,
            [Description("-lastName")] LastNameDesc,
            [Description("-firstName")] FirstNameDesc,
            [Description("-middleName")] MiddleNameDesc,
            [Description("-suffix")] SuffixDesc,
            [Description("-modified")] ModifiedDesc,
        }

        #endregion

        #endregion

        #region constructor

        public Creator()
        {
        }

        public Creator(int creatorId)
        {
            this.Id = creatorId;
        }

        #endregion

        #region methods

        /// <summary>
        /// Get list of creators by their first name, max of 100 results.
        /// </summary>
        /// <param name="firstName">Creators first name to search by</param>
        /// <param name="lastName">Creators last name to search by</param>
        /// <param name="orderBy">Look at Creator.OrderBy enum for options.</param>
        /// <returns>List of creators with provided first name.</returns>
        public static List<Creator> GetCreatorsByFirstOrLastName(string firstName, string lastName, OrderBy orderBy)
        {
            Dictionary<string, string> parameters;
            JArray array;
            if (!firstName.Equals("") && lastName.Equals(""))
            {
                parameters = new Dictionary<string, string> {{"firstName", firstName}, {"orderBy", Utilities.GetEnumDescription(orderBy)}};
                array = Utilities.GetJArray("creators", 100, 0, false, parameters);
                return new List<Creator>(array.Select(token => JsonConvert.DeserializeObject<Creator>(token.ToString())));
            }
            if (!lastName.Equals("") && firstName.Equals(""))
            {
                parameters = new Dictionary<string, string> {{"lastName", lastName}, {"orderBy", Utilities.GetEnumDescription(orderBy)}};
                array = Utilities.GetJArray("creators", 100, 0, false, parameters);
                return new List<Creator>(array.Select(token => JsonConvert.DeserializeObject<Creator>(token.ToString())));
            }
            if (!lastName.Equals("") && !firstName.Equals(""))
            {
                return new List<Creator> {GetCreatorByFullName(firstName, lastName)};
            }
            throw new Exception("First or last name must be provided");            
        } 

        /// <summary>
        /// Get a specific creator with provided first and last name
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns>Deserialized Creator object</returns>
        public static Creator GetCreatorByFullName(string firstName, string lastName)
        {
            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            {
                var parameters = new Dictionary<string, string> {{"firstName", firstName}, {"lastName", lastName}};
                return Utilities.GetJObject<Creator>("creators",parameters);
            }
            throw new ArgumentException("Please provide an first and last name to search for, otherwise use the methods that return lists.");
        }

        public static List<Creator> CreatorGetByPartialName(string creatorName = null, int limit = 100, bool getAll = true)
        {
            if (!string.IsNullOrEmpty(creatorName))
            {
                var parameters = new Dictionary<string, string>
                {
                    {"nameStartsWith",creatorName},
                };
                var array = Utilities.GetJArray("creators", limit, 0, getAll, parameters);
                return new List<Creator>(array.Select(token => JsonConvert.DeserializeObject<Creator>(token.ToString())));
            }
            throw new ArgumentException("Please provide an ID to search for");
        } 

        public static Creator GetCreatorById(int id)
        {
            return Utilities.GetJObject<Creator>("creators/{id}", id);
        }

        public List<Comic> GetComicsByCreator(int limit = 100, int offset = 0, bool getAll = false)
        {
            return Comic.GetComicsByCreatorId(this.Id, limit, offset, getAll);
        }

        public List<Event> GetEventsByCreator(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("creators/{id}/events", limit, offset, getAll, parameters);
            return new List<Event>(array.Select(t => JsonConvert.DeserializeObject<Event>(t.ToString())));
        }

        public List<Series.Series> GetSeriesByCreator(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string>{{"id",Id.ToString()}};
            var array = Utilities.GetJArray("creators/{id}/series", limit, offset, getAll, parameters);
            return new List<Series.Series>(array.Select(t => JsonConvert.DeserializeObject<Series.Series>(t.ToString())));
        }

        public List<Story> GetStoriesByCreator(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("creators/{id}/stories", limit, offset, getAll, parameters);
            return new List<Story>(array.Select(t => JsonConvert.DeserializeObject<Story>(t.ToString())));
        }
        
        #endregion

        #region test

        [Fact]
        public void CreatorTestGetByName()
        {
            var c = GetCreatorByFullName("Dan", "Slott");
            Console.WriteLine("First: {0} Last: {1}", c.FirstName, c.LastName);
            Assert.True(c.FirstName.Equals("Dan") && c.LastName.Equals("Slott"), 
                "Creator was not successfully retrieved");
        }

        [Fact]
        public void CreatorTestGetByPartialName()
        {
            List<Creator> creators = CreatorGetByPartialName("bri");
            foreach (Creator creator in creators)
            {
                Console.WriteLine(creator.FullName);
            }
            Assert.True(creators.Count > 0, "Failed to get a list of creators");
        }

        [Fact]
        public void CreatorTestGetByFirstAndLast()
        {
            var creators = GetCreatorsByFirstOrLastName("Dan", "Slott", OrderBy.FirstNameAsc);
            Assert.True(creators.Count == 1, "Got more than one result");
            Assert.True(creators[0].FirstName.Equals("Dan") && creators[0].LastName.Equals("Slott"), "Failed to get proper result.");
        }

        [Fact]
        public void CreatorTestGetAllByFirstName()
        {
            var creators = GetCreatorsByFirstOrLastName("Dan", "", OrderBy.FirstNameAsc);
            Console.WriteLine(creators.Count);
            creators.ForEach(e => Console.WriteLine("{0} {1}", e.FirstName, e.LastName));
            Assert.True(creators.Count > 0, "Failed to get any results.");
        }

        [Fact]
        public void CreatorTestGetAllByLastName()
        {
            var creators = GetCreatorsByFirstOrLastName("", "Hickman", OrderBy.FirstNameAsc);
            Console.WriteLine(creators.Count);
            creators.ForEach(e => Console.WriteLine("{0} {1}", e.FirstName, e.LastName));
            Assert.True(creators.Count > 0, "Failed to get any results.");
        }

        [Fact]
        public void CreatorTestGetById()
        {
            var c = GetCreatorById(24);
            foreach (var item in c.Comics.Items)
            {
                Console.WriteLine(item.Name);
            }
            Assert.True(c.FirstName.Equals("Brian"), "first name not match");
            Assert.True(c.LastName.Equals("Bendis"), "last name not match");
        }

        // will be using 24 for the ID for these.. this is Brian Michael Bendis
        [Fact]
        public void CreatorTestGetComicsByCreator()
        {
            Creator creator = new Creator(24);
            List<Comic> comics = creator.GetComicsByCreator(50);
            foreach (var comic in comics)
            {
                Console.WriteLine(comic.Title);
            }
            Assert.True(comics.Count > 0 && comics.Count <= 50, "Failed to get a list of comics by creator: " + 24);
        }

        [Fact]
        public void CreatorTestGetStoriesByCreator()
        {
            Creator cre = new Creator(24);
            List<Story> stories = cre.GetStoriesByCreator();
            foreach (Story story in stories)
            {
                Console.WriteLine(story.Title);
            }
            Assert.True(stories.Count > 0);
        }

        [Fact]
        public void CreatorTestGetSeriesByCreator()
        {
            Creator cre = new Creator(24);
            List<Series.Series> series = cre.GetSeriesByCreator(50);
            foreach (Series.Series se in series)
            {
                Console.WriteLine(se.Title);
            }
            Assert.True(series.Count > 0 && series.Count <= 50, "Failed to get an appropriate list of series.");
        }

        [Fact]
        public void CreatorTestGetEventsByCreator()
        {
            Creator cre = new Creator(24);
            List<Event> events = cre.GetEventsByCreator(50);
            foreach (Event se in events)
            {
                Console.WriteLine(se.Title);
            }
            Assert.True(events.Count > 0 && events.Count <= 50, "Failed to get an appropriate list of events.");
        }

        #endregion
    }
}