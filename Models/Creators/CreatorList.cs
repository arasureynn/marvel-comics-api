﻿namespace MarvelSharp.Models.Creators
{
    public class CreatorList
    {
        public int Available { get; set; }
        public int Returned { get; set; }
        public dynamic CollectionURI { get; set; }
        public CreatorSummary[] Items { get; set; }

        public CreatorList()
        {
            
        }
    }
}
