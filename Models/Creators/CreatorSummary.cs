﻿namespace MarvelSharp.Models.Creators
{
    public class CreatorSummary
    {
        public string ResourceURI { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }

        public CreatorSummary()
        {
            
        }
    }
}
