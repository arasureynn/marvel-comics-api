﻿namespace MarvelSharp.Models.Creators
{
    public class CreatorDataContainer
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public int total { get; set; }
        public int count { get; set; }
        public Creator[] results { get; set; }

        public CreatorDataContainer()
        {
            
        }
    }
}
