﻿namespace MarvelSharp.Models.Creators
{
    public class CreatorDataWrapper
    {
        public int code { get; set; }
        public string status { get; set; }
        public CreatorDataContainer data { get; set; }
        public string etag { get; set; }

        public CreatorDataWrapper()
        {
            
        }
    }
}
