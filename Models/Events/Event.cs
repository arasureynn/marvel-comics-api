﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using MarvelSharp.Models.Characters;
using MarvelSharp.Models.Comics;
using MarvelSharp.Models.Creators;
using MarvelSharp.Models.Series;
using MarvelSharp.Models.Stories;
using Newtonsoft.Json;
using Xunit;

namespace MarvelSharp.Models.Events
{
    public class Event
    {
        #region properties

        public enum OrderBy
        {
            [Description("name")] NameAsc,
            [Description("startDate")] StartDateAsc,
            [Description("modified")] ModifiedAsc,
            [Description("-name")] NameDesc,
            [Description("-startDate")] StartDateDesc,
            [Description("-modified")] ModifiedDesc
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ResourceUri { get; set; }
        public Url[] Urls { get; set; }
        public string Modified { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public Image Thumbnail { get; set; }
        public ComicList Comics { get; set; }
        public StoryList Stories { get; set; }
        public SeriesList Series { get; set; }
        public CharacterList Characters { get; set; }
        public CreatorList Creators { get; set; }
        public EventSummary Next { get; set; }
        public EventSummary Previous { get; set; }

        private static List<Event> _eventsList; 

        #endregion

        #region constructor

        public Event()
        {
            
        }

        #endregion

        #region methods

        public static List<Event> EventsGetList(int limit = 100, int offset = 0, bool getAll = false, OrderBy orderBy = OrderBy.NameAsc)
        {
            var parameters = new Dictionary<string, string>
            {
                {"orderBy", Utilities.GetEnumDescription(orderBy)}
            };
            var array = Utilities.GetJArray("events", limit, offset, getAll, parameters);
            return new List<Event>(array.Select(token => JsonConvert.DeserializeObject<Event>(token.ToString())));
        }

        public static List<Event> EventsGetByPartialName(string eventName = null, int limit = 100, bool getAll = true)
        {
            if (!string.IsNullOrEmpty(eventName))
            {
                var parameters = new Dictionary<string, string>
                {
                    {"nameStartsWith", eventName},
                };
                var array = Utilities.GetJArray("events", parameters: parameters);
                return new List<Event>(array.Select(token => JsonConvert.DeserializeObject<Event>(token.ToString())));
            }
            throw new ArgumentException("Please provide an ID to search for");
        } 

        public static Event EventGetByName(string eventTitle)
        {
            if (!eventTitle.Equals(""))
            {
                var parameters = new Dictionary<string, string>
                {
                    {"name",eventTitle},
                };
                return Utilities.GetJObject<Event>("events", parameters);
            }
            throw new ArgumentException("Please provide an ID to search for");
        }

        public static Event EventGetById(int eventId)
        {
            if (eventId != 0)
            {
                return Utilities.GetJObject<Event>("events/{id}", eventId);
            }
            throw new ArgumentException("Please provide an ID to search for");
        }

        public List<Character> GetCharactersByEvent(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string>{{"id",Id.ToString()}};
            var array = Utilities.GetJArray("events/{id}/characters", limit, offset, getAll, parameters);
            return new List<Character>(array.Select(t => JsonConvert.DeserializeObject<Character>(t.ToString())));
        }

        public List<Comic> GetComicsByEvent(int limit = 100, int offset = 0, bool getAll = false, Comic.ComicFormat format = Comic.ComicFormat.Comic)
        {
            var parameters = new Dictionary<string, string>
            {
                {"id", Id.ToString()},
                {"format", Utilities.GetEnumDescription(format)}
            };
            var array = Utilities.GetJArray("events/{id}/comics", limit, offset, getAll, parameters);
            return new List<Comic>(array.Select(t => JsonConvert.DeserializeObject<Comic>(t.ToString())));
        }

        public List<Creator> GetCreatorsByEvent(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("events/{id}/creators", limit, offset, getAll, parameters);
            return new List<Creator>(array.Select(t => JsonConvert.DeserializeObject<Creator>(t.ToString())));
        }

        public List<Series.Series> GetSeriesByEvent(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("events/{id}/series", limit, offset, getAll, parameters);
            return new List<Series.Series>(array.Select(t => JsonConvert.DeserializeObject<Series.Series>(t.ToString())));
        }

        public List<Story> GetStoriesByEvent(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("events/{id}/stories", limit, offset, getAll, parameters);
            return new List<Story>(array.Select(t => JsonConvert.DeserializeObject<Story>(t.ToString())));
        }

            #endregion

        #region tests

        [Fact]
        public void EventTestGetByName()
        {
            Event e = EventGetByName("Age Of Ultron");
            Console.WriteLine("Id: {0}", e.Id);
            Assert.True(e.Id == 314, "Could not load, id found was: " + e.Id);
            Assert.True(e.Title.ToLower().Equals("age of ultron"), "Incorrect title loaded, received: " + e.Title);
        }

        [Fact]
        public void EventTestGetById()
        {
            Event e = EventGetById(314);
            Console.WriteLine("Title: {0}", e.Title);
            Assert.True(e.Id == 314, "Did not load from ID: " + e.Id);
            Assert.True(e.Title.ToLower().Equals("age of ultron"), "Did not get proper title: " + e.Title);
        }

        [Fact]
        public void EventGetPartial()
        {
            List<Event> events = EventsGetByPartialName("infinity");
            foreach (var @event in events)
            {
                Console.WriteLine(@event.Title);
            }
            Assert.True(events.Count > 0, "No events found starting with infinity");
        }

        [Fact]
        public void EventTestGetList()
        {
            List<Event> events = EventsGetList();
            events.ForEach(e => Console.WriteLine(e.Title));
            Assert.True(events.Count <= 100 && events.Count > 0, "Returned value was : " + events.Count);
        }

        #endregion
    }
}