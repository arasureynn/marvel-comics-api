namespace MarvelSharp.Models.Events
{
	public class EventList
	{
		public int Available { get; set; }
		public int Returned { get; set; }
		public dynamic CollectionURI { get; set; }
		public EventSummary[] Items { get; set; }


		public EventList ()
		{
		}
	}
}