﻿namespace MarvelSharp.Models.Events
{
    public class EventDataWrapper
    {
        public int code { get; set; }
        public string status { get; set; }
        public EventDataContainer data { get; set; }
        public string etag { get; set; }

        public EventDataWrapper()
        {
            
        }
    }
}
