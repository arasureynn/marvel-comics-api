﻿namespace MarvelSharp.Models.Events
{
    public class EventDataContainer
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public int total { get; set; }
        public int count { get; set; }
        public Event[] results { get; set; }

        public EventDataContainer()
        {
            
        }
    }
}
