﻿namespace MarvelSharp.Models.Events
{
    public class EventSummary
    {
        public string available { get; set; }
        public string name { get; set; }

        public EventSummary()
        {
            
        }
    }
}
