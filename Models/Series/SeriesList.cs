namespace MarvelSharp.Models.Series
{
	public class SeriesList
	{
		public int Available { get; set; }
		public int Returned { get; set; }
		public dynamic CollectionURI { get; set; }
		public SeriesSummary[] Items { get; set; }


		public SeriesList ()
		{
		}
	}
}

