﻿namespace MarvelSharp.Models.Series
{
    public class SeriesDataWrapper
    {
        public int code { get; set; }
        public string status { get; set; }
        public SeriesDataContainer data { get; set; }
        public string etag { get; set; }

        public SeriesDataWrapper()
        {
            
        }
    }
}
