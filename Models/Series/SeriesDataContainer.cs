﻿namespace MarvelSharp.Models.Series
{
    public class SeriesDataContainer
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public int total { get; set; }
        public int count { get; set; }
        public Series[] results { get; set; }

        public SeriesDataContainer()
        {
            
        }
    }
}
