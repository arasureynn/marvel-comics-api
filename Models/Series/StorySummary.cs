﻿namespace MarvelSharp.Models.Series
{
    public class SeriesSummary
    {
        public string resourceURI { get; set; }
        public string name { get; set; }


        public SeriesSummary() { }
    }
}
