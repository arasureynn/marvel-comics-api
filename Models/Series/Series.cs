﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using MarvelSharp.Models.Characters;
using MarvelSharp.Models.Comics;
using MarvelSharp.Models.Creators;
using MarvelSharp.Models.Events;
using MarvelSharp.Models.Stories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MarvelSharp.Models.Series
{
    public class Series
    {
        #region properties

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ResourceURI { get; set; }
        public Url[] Urls { get; set; }
        public int StartYear { get; set; }
        public int EndYear { get; set; }
        public string Rating { get; set; }
        public string Modified { get; set; }
        public Image Thumbnail { get; set; }
        public ComicList Comics { get; set; }
        public StoryList Stories { get; set; }
        public EventList Events { get; set; }
        public CharacterList Characters { get; set; }
        public CreatorList Creators { get; set; }
        public SeriesSummary Next { get; set; }
        public SeriesSummary Previous { get; set; }

        #region ENUMS

        public enum SeriesType
        {
            [Description("ongoing")] Ongoing,
            [Description("limited")] Limited,
            [Description("oneShot")] OneShot,
            [Description("collection")] Collection
        }

        public enum SeriesContains
        {
            [Description("comic")] Comic,
            [Description("magazine")] Magazine,
            [Description("trade paperback")] TradePaperback,
            [Description("hardcover")] Hardcover,
            [Description("digest")] Digest,
            [Description("graphic novel")] GraphicNovel,
            [Description("digital comic")] DigitalComic,
            [Description("infinite comic")] InfiniteComic,
        }

        public enum SeriesSortOrder
        {
            [Description("title")]
            TitleAsc,
            [Description("modified")]
            ModifiedAsc,
            [Description("startYear")]
            StartYearAsc,
            [Description("-title")]
            TitleDesc,
            [Description("-modified")]
            ModifiedDesc,
            [Description("-startYear")]
            StartYearDesc
        }

        #endregion

        #endregion

        #region constructor

        public Series()
        {

        }

        #endregion

        #region methods

        /// <summary>
        /// Gets a full list of series.
        /// </summary>
        /// <param name="limit">how many series to retrieve each time, if getAll is true this should be 100 which is the max.</param>
        /// <param name="offset">which number series we are going to start with</param>
        /// <param name="getAll">Whether or not to keep looping through until all series are done, this can take a very long time</param>
        /// <param name="contains">useUtilities.GetDescription(Series.SeriesContains.*)</param>
        /// <returns></returns>
        public static List<Series> GetAllSeries(int limit, int offset, bool getAll, SeriesContains contains)
        {
            var parameters = new Dictionary<string, string> { {"contains", Utilities.GetEnumDescription(contains)} };
            JArray array = Utilities.GetJArray("series", limit, offset, getAll, parameters);
            return new List<Series>(array.Select(token => JsonConvert.DeserializeObject<Series>(token.ToString())));
        }

        public static List<Series> GetSeriesByPartialName(string name, int limit, int offset, bool getAll, SeriesContains contains)
        {

            var parameters = new Dictionary<string, string>
            {
                {"contains", Utilities.GetEnumDescription(contains)},
                {"nameStartsWith", name}
            };
            JArray array = Utilities.GetJArray("series", limit, offset, getAll, parameters);
            return new List<Series>(array.Select(token => JsonConvert.DeserializeObject<Series>(token.ToString())));
        } 

        /// <summary>
        /// This would be for getting all the series with Wolverine in the title for instance, this is an exact match, so X-Men will only find titles that are exactly X-Men, it will not find Uncanny X-Men or Amazing X-Men
        /// </summary>
        /// <param name="title">Title you are searching for</param>
        /// <param name="limit">how many series to retrieve each time, if getAll is true this should be 100 which is the max.</param>
        /// <param name="offset">which number series we are going to start with</param>
        /// <param name="getAll">Whether or not to keep looping through until all series are done, this can take a very long time</param>
        /// <param name="contains">useUtilities.GetDescription(Series.SeriesContains.*)</param>
        /// <returns></returns>
        public static List<Series> GetMultipleSeriesByTitle(string title, int limit, int offset,
            SeriesContains contains)
        {
            var parameters = new Dictionary<string, string>
            {
                {"title", title},
                {"contains", Utilities.GetEnumDescription(contains)}
            };
            var array = Utilities.GetJArray("series", limit, offset, false, parameters);
            // Convert json data to Character object, add to Character list to be returned later
            return new List<Series>(array.Select(token => JsonConvert.DeserializeObject<Series>(token.ToString())));
        }

        /// <summary>
        /// Gets only the Ongoing series
        /// </summary>
        /// <param name="limit">how many series to retrieve each time, if getAll is true this should be 100 which is the max.</param>
        /// <param name="offset">which number series we are going to start with</param>
        /// <param name="getAll">Whether or not to keep looping through until all series are done, this can take a very long time</param>
        /// <param name="contains">useUtilities.GetDescription(Series.SeriesContains.*)</param>
        /// <returns></returns>
        public static List<Series> GetOngoingSeries(int limit, int offset, bool getAll, SeriesContains contains)
        {
            var parameters = new Dictionary<string, string>
            {
                {"seriesType", Utilities.GetEnumDescription(SeriesType.Ongoing)},
                {"contains", Utilities.GetEnumDescription(contains)}
            };
            
            var array = Utilities.GetJArray("series", limit, offset, getAll, parameters);

            return new List<Series>(array.Select(token => JsonConvert.DeserializeObject<Series>(token.ToString())));
        }

        /// <summary>
        /// Gets only the Limited series
        /// </summary>
        /// <param name="limit">how many series to retrieve each time, if getAll is true this should be 100 which is the max.</param>
        /// <param name="offset">which number series we are going to start with</param>
        /// <param name="getAll">Whether or not to keep looping through until all series are done, this can take a very long time</param>
        /// <param name="contains">useUtilities.GetDescription(Series.SeriesContains.*)</param>
        /// <returns></returns>
        public static List<Series> GetLimitedSeries(int limit, int offset, bool getAll, SeriesContains contains)
        {
            var parameters = new Dictionary<string, string>
            {
                {"seriesType", Utilities.GetEnumDescription(SeriesType.Limited)},
                {"contains", Utilities.GetEnumDescription(contains)}
            };
            var array = Utilities.GetJArray("series", limit, offset, getAll, parameters);
            return new List<Series>(array.Select(t => JsonConvert.DeserializeObject<Series>(t.ToString())));
        }

        /// <summary>
        /// Search for a specific series based on an ID from Marvel database.
        /// </summary>
        /// <param name="seriesId">ID of the series from the Marvel database.</param>
        /// <returns></returns>
        public static Series GetSeriesById(int seriesId)
        {
            if (seriesId != 0)
            {
                return Utilities.GetJObject<Series>("series/{id}", seriesId);
            }
            throw new ArgumentException("Please provide an ID to search for");
        }

        /// <summary>
        /// Search for a series by name, should not include publish/run years
        /// Example, Search for Wolverine Max not Wolverine Max (2013)
        /// </summary>
        /// <param name="title">Title of series.</param>
        /// <returns></returns>
        public static Series GetSeriesByTitle(string title)
        {
            if (!string.IsNullOrEmpty(title))
            {
                var parameters = new Dictionary<string, string>
                {
                    {"title", title}
                };

                return Utilities.GetJObject<Series>("series", parameters);
            }
            throw new ArgumentException("Please provide an title to search for");
        }

        public List<Character> GetCharactersBySeries(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("series/{id}/characters", limit, offset, getAll, parameters);
            return new List<Character>(array.Select(t => JsonConvert.DeserializeObject<Character>(t.ToString())));
        }

        public List<Comic> GetComicsBySeries(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { { "id", Id.ToString() } };
            var array = Utilities.GetJArray("series/{id}/comics", limit, offset, getAll, parameters);
            return new List<Comic>(array.Select(t => JsonConvert.DeserializeObject<Comic>(t.ToString())));
        }

        public List<Creator> GetCreatorsBySeries(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { { "id", Id.ToString() } };
            var array = Utilities.GetJArray("series/{id}/creators", limit, offset, getAll, parameters);
            return new List<Creator>(array.Select(t => JsonConvert.DeserializeObject<Creator>(t.ToString())));
        }

        public List<Event> GetEventsBySeries(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { { "id", Id.ToString() } };
            var array = Utilities.GetJArray("series/{id}/events", limit, offset, getAll, parameters);
            return new List<Event>(array.Select(t => JsonConvert.DeserializeObject<Event>(t.ToString())));
        }

        public List<Story> GetStoriesBySeries(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { { "id", Id.ToString() } };
            var array = Utilities.GetJArray("series/{id}/stories", limit, offset, getAll, parameters);
            return new List<Story>(array.Select(t => JsonConvert.DeserializeObject<Story>(t.ToString())));
        }

            #endregion

        #region tests

        [Fact]
        public void SeriesTestGetById()
        {
            Series s = GetSeriesById(9425);
            Assert.True(s.Title.Equals("Dark X-Men (2010)"));
        }

        [Fact]
        public void SeriesTestGetByName()
        {
            Series s = GetSeriesByTitle("Wolverine Max");
            Console.WriteLine("Title: {0}", s.Title);
            foreach (CreatorSummary c in s.Creators.Items)
            {
                Console.WriteLine(c.Name);
            }
            Assert.True(s.Id == 17620, "Id did not match expected results");
            Assert.True(s.Title.Equals("Wolverine Max (2013)"));
        }

        [Fact]
        public void SeriesTestGetMultipleByTitle()
        {
            List<Series> series = GetMultipleSeriesByTitle("Wolverine", 100, 0, SeriesContains.Comic);
            Console.WriteLine("Count: {0}", series.Count);
            Assert.True(series.Count > 0, "Failed to get wolverine series");
        }

        [Fact]
        public void SeriesTestGetLimited()
        {
            List<Series> ongoings = GetOngoingSeries(100, 0, false, SeriesContains.Comic);
            Console.WriteLine("Count: {0}", ongoings.Count);
            Assert.True(ongoings.Count > 0, "failed to get full list of ongoing comics");
        }

        [Fact]
        public void SeriesTestGetOngoing()
        {
            List<Series> limited = GetLimitedSeries(100, 0, false, SeriesContains.Comic);
            Console.WriteLine("Count: {0}", limited.Count);
            Assert.True(limited.Count > 0, "Failed to get list of limited comics");
        }

        [Fact]
        public void SeriesTestGetAll()
        {
            List<Series> all = GetAllSeries(100, 200, false, SeriesContains.Comic);
            Console.WriteLine("Count: {0}", all.Count);
            Assert.True(all.Count > 0, "Failed to get a list of all comics");
        }

        #endregion
    }
}
