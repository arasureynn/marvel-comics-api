﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using MarvelSharp.Models.Characters;
using MarvelSharp.Models.Creators;
using MarvelSharp.Models.Events;
using MarvelSharp.Models.Series;
using MarvelSharp.Models.Stories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MarvelSharp.Models.Comics
{
    public class Comic
    {
        #region properties
        
        public int Id { get; set; }
        public int DigitalId { get; set; }
        public string Title { get; set; }
        public string IssueNumber { get; set; }
        public string VariantDescription { get; set; }
        public string Description { get; set; }
        public string Modified { get; set; }
        public string Isbn { get; set; }
        public string Upc { get; set; }
        public string DiamondCode { get; set; }
        public string Ean { get; set; }
        public string Issn { get; set; }
        public string Format { get; set; }
        public int PageCount { get; set; }
        public TextObject[] TextObjects { get; set; }
        public string ResourceURI { get; set; }
        public Url[] Urls { get; set; }
        public SeriesSummary Series { get; set; }
        public ComicSummary[] Variants { get; set; }
        public ComicSummary[] Collections { get; set; }
        public ComicSummary[] CollectedIssues { get; set; }
        public ComicDate[] Dates { get; set; }
        public ComicPrice[] Prices { get; set; }
        public Image Thumbnail { get; set; }
        public Image[] Images { get; set; }
        public CreatorList Creators { get; set; }
        public CharacterList Characters { get; set; }
        public StoryList Stories { get; set; }
        public EventList Events { get; set; }

        #region ENUMS

        public enum ComicFormat
        {
            [Description("comic")]
            Comic,
            [Description("magazine")]
            Magazine,
            [Description("trade paperback")]
            TradePaperback,
            [Description("hardcover")]
            Hardcover,
            [Description("digest")]
            Digest,
            [Description("graphic novel")]
            GraphicNovel,
            [Description("digital comic")]
            DigitalComic,
            [Description("infinite comic")]
            InfiniteComic
        }

        #endregion

        #endregion

        #region constructor

        public Comic()
        {
        }

        public Comic(int comicId)
        {
            this.Id = comicId;
        }

        #endregion

        #region methods

        public static Comic GetComicById(int comicId)
        {
            return Utilities.GetJObject<Comic>("comics/{id}", comicId);
        }

        public static List<Comic> GetComics(int limit = 100, int offset = 0, bool getAll = false, ComicFormat format = ComicFormat.Comic)
        {
            var parameters = new Dictionary<string, string>{{"format", Utilities.GetEnumDescription(format)}};
            var array = Utilities.GetJArray("comics", limit, offset, getAll, parameters);
            return new List<Comic>(array.Select(t => JsonConvert.DeserializeObject<Comic>(t.ToString())));
        } 

        public static List<Comic> GetComicsBySeriesId(int limit = 100, int offset = 0, bool getAll = false, int series = 0)
        {
            var parameters = new Dictionary<string, string> { {"id", series.ToString()} };
            JArray array = Utilities.GetJArray("series/{id}/comics", limit, offset, getAll, parameters);
            return new List<Comic>(array.Select(token => JsonConvert.DeserializeObject<Comic>(token.ToString())));
        }

        public static List<Comic> GetComicsByCreatorId(int creatorId = 0, int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", creatorId.ToString()}};
            JArray array = Utilities.GetJArray("creators/{id}/comics", limit, offset, getAll, parameters);
            return new List<Comic>(array.Select(token => JsonConvert.DeserializeObject<Comic>(token.ToString())));
        }

        public List<Creator> GetCreatorsByComicId(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { {"id", Id.ToString()} };
            JArray array = Utilities.GetJArray("comics/{id}/creators", limit, offset, getAll, parameters);
            return new List<Creator>(array.Select(token => JsonConvert.DeserializeObject<Creator>(token.ToString())));
        }

        public List<Character> GetCharactersByComicId(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { {"id", Id.ToString()} };
            JArray array = Utilities.GetJArray("comics/{id}/characters", limit, offset, getAll, parameters);
            return new List<Character>(array.Select(token => JsonConvert.DeserializeObject<Character>(token.ToString())));
        }

        public List<Event> GetEventsByComicId(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string>{ {"id", Id.ToString()} };
            JArray array = Utilities.GetJArray("comics/{id}/events", limit, offset, getAll, parameters);
            return new List<Event>(array.Select(token => JsonConvert.DeserializeObject<Event>(token.ToString())));
        }

        public List<Story> GetStoriesByComicId(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { {"id", Id.ToString()} };
            JArray array = Utilities.GetJArray("comics/{id}/stories", limit, offset, getAll, parameters);
            return new List<Story>(array.Select(token => JsonConvert.DeserializeObject<Story>(token.ToString())));
        }
        
        #endregion

        #region tests

        [Fact]
        public void ComicsTestGetComics()
        {
            var comics = GetComics(50);
            Assert.True(comics.Count == 50, "Failed to get a full list of format: comics");
            comics = GetComics(50, format: ComicFormat.Digest);
            Assert.True(comics.Count == 50, "Failed to get a full list of format: digest");
            comics = GetComics(50, format: ComicFormat.DigitalComic);
            Assert.True(comics.Count == 50, "Failed to get a full list of format: digital");
            comics = GetComics(50, format: ComicFormat.GraphicNovel);
            Assert.True(comics.Count == 50, "Failed to get a full list of format: graphic");
            comics = GetComics(50, format: ComicFormat.Hardcover);
            Assert.True(comics.Count == 50, "Failed to get a full list of format: hardcover");
            comics = GetComics(50, format: ComicFormat.Magazine);
            Assert.True(comics.Count == 50, "Failed to get a full list of format: magazine");
            comics = GetComics(50, format: ComicFormat.TradePaperback);
            Assert.True(comics.Count == 50, "Failed to get a full list of format: trades");
        }

        [Fact]
        public void ComicsTestGetById()
        {
            Comic c = GetComicById(48633);
            Console.WriteLine("Creators");
            foreach (CreatorSummary creator in c.Creators.Items)
            {
                Console.WriteLine(creator.Name);
            }
            Assert.True(c.Title.Equals("Guardians of the Galaxy (2013) #14"), 
                "Failed to get the correct Comic. " + c.Title);
        }

        [Fact]
        public void ComicsTestGetBySeries()
        {
            List<Comic> comics = GetComicsBySeriesId(100, 0, false, 17900);
            Console.WriteLine("Total: {0}", comics.Count);
            comics.ForEach(e => Console.WriteLine(e.Title));
            Assert.True(comics.Count > 0, "Did not get any results when searching for Age Of Ultron");
        }

        [Fact]
        public void ComicsTestGetByCreatorId()
        {
            List<Comic> comics = GetComicsByCreatorId(24, getAll:false);
            comics.ForEach(e => Console.WriteLine(e.Title));
            Assert.True(comics.Count > 0, "Did not get any results when searching for creator ID 24 (Brian Michael Bendis)");
        }

        [Fact]
        public void ComicTestGetEventsById()
        {
            Comic comic = new Comic(43297);
            List<Event> events = comic.GetEventsByComicId(50);
            foreach (Event ev in events)
            {
                Console.WriteLine(ev.Title);
            }
            Assert.True(events.Count > 0 && events.Count <= 50, "Failed to get a list of events");
        }

        // using ID 47810 for Inhuman Issue 2
        [Fact]
        public void ComicTestGetCreatorsById()
        {
            Comic comic = new Comic(47810);
            List<Creator> creators = comic.GetCreatorsByComicId(50);
            foreach (Creator ev in creators)
            {
                Console.WriteLine(ev.FullName);
            }
            Assert.True(creators.Count > 0 && creators.Count <= 50, "Failed to get a list of creators");
        }

        [Fact]
        public void ComicTestGetStoriesById()
        {
            Comic comic = new Comic(47810);
            List<Story> stories = comic.GetStoriesByComicId(50);
            foreach (Story ev in stories)
            {
                Console.WriteLine(ev.Title);
            }
            Assert.True(stories.Count > 0 && stories.Count <= 50, "Failed to get a list of stories");
        }

        [Fact]
        public void ComicTestGetCharactersById()
        {
            Comic comic = new Comic(50247); // this is the only comic I could find with a character list.
            List<Character> characters = comic.GetCharactersByComicId(50);
            foreach (Character ev in characters)
            {
                Console.WriteLine(ev.Name);
            }
            Assert.True(characters.Count > 0 && characters.Count <= 50, "Failed to get a list of creators");
        }

        #endregion
    }

    public class TextObject
    {
        public string Type { get; set; }
        public string Language { get; set; }
        public string Text { get; set; }
    }

    public class ComicDate
    {
        public string Type { get; set; }
        public string Date { get; set; }
    }

    public class ComicPrice
    {
        public string Type { get; set; }
        public float Price { get; set; }
    }
}