namespace MarvelSharp.Models.Comics
{
	public class ComicList
	{
		public int Available { get; set; }
		public int Returned { get; set; }
		public dynamic CollectionURI { get; set; }
		public ComicSummary[] Items { get; set; }


		public ComicList ()
		{
		}
	}
}

