﻿namespace MarvelSharp.Models.Comics
{
    class ComicDataContainer
    {
        public int Offset { get; set; }
        public int Limit { get; set; }
        public int Total { get; set; }
        public int Count { get; set; }
        public Comic[] Results { get; set; }

        public ComicDataContainer()
        {
            
        }
    }
}
