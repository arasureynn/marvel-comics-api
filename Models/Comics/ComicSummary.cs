﻿namespace MarvelSharp.Models.Comics
{
    public class ComicSummary
    {
        public string ResourceURI { get; set; }
        public string Name { get; set; }


        public ComicSummary()
        {
        }
    }
}
