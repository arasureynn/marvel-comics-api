﻿namespace MarvelSharp.Models.Comics
{
    class ComicDataWrapper
    {
        public int Code { get; set; }
        public int Status { get; set; }
        public ComicDataContainer Data { get; set; }
        public string Etag { get; set; }

        public ComicDataWrapper()
        {
            
        }
    }
}
