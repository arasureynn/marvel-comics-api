﻿namespace MarvelSharp.Models.Stories
{
    public class StoryDataContainer
    {
        public int Offset { get; set; }
        public int Limit { get; set; }
        public int Total { get; set; }
        public int Count { get; set; }
        public Story[] Results { get; set; }

        public StoryDataContainer()
        {
            
        }
    }
}
