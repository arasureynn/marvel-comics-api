using Newtonsoft.Json;

namespace MarvelSharp.Models.Stories
{
	public class StoryList
	{
		public int Available { get; set; }
		public int Returned { get; set; }
		public dynamic CollectionURI { get; set; }
		public StorySummary[] Items { get; set; }

		public StoryList ()
		{
		}
	}
}

