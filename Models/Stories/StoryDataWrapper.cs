﻿namespace MarvelSharp.Models.Stories
{
    public class StoryDataWrapper
    {
        public int Code { get; set; }
        public string Status { get; set; }
        public StoryDataContainer Data { get; set; }
        public string Etag { get; set; }

        public StoryDataWrapper()
        {
            
        }
    }
}
