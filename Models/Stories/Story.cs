﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarvelSharp.Models.Characters;
using MarvelSharp.Models.Comics;
using MarvelSharp.Models.Creators;
using MarvelSharp.Models.Events;
using MarvelSharp.Models.Series;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MarvelSharp.Models.Stories
{
    public class Story
    {
        #region properties

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ResourceURI { get; set; }
        public string Type { get; set; }
        public string Modified { get; set; }
        public Image Thumbnail { get; set; }
        public ComicList Comics { get; set; }
        public SeriesList Series { get; set; }
        public EventList Events { get; set; }
        public CharacterList Characters { get; set; }
        public CreatorList Creators { get; set; }
        public ComicSummary Originalissue { get; set; }

        #endregion

        #region constructor

        public Story()
        {

        }

        #endregion

        #region methods

        public static List<Story> GetStories(int limit = 100, int offset = 0, bool getAll = true)
        {
            var array = Utilities.GetJArray("stories", limit, offset, getAll);
            return new List<Story>(array.Select(token => JsonConvert.DeserializeObject<Story>(token.ToString())));
        }

        public static Story GetStoryById(int storyId)
        {
            return Utilities.GetJObject<Story>("stories/{id}", storyId);
        }

        public List<Character> GetCharactersByStory(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> {{"id", Id.ToString()}};
            var array = Utilities.GetJArray("stories/{id}/characters", limit, offset, getAll, parameters);
            return new List<Character>(array.Select(t => JsonConvert.DeserializeObject<Character>(t.ToString())));
        }

        public List<Comic> GetComicsByStory(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { { "id", Id.ToString() } };
            var array = Utilities.GetJArray("stories/{id}/comics", limit, offset, getAll, parameters);
            return new List<Comic>(array.Select(t => JsonConvert.DeserializeObject<Comic>(t.ToString())));
        }

        public List<Creator> GetCreatorsByStory(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { { "id", Id.ToString() } };
            var array = Utilities.GetJArray("stories/{id}/creators", limit, offset, getAll, parameters);
            return new List<Creator>(array.Select(t => JsonConvert.DeserializeObject<Creator>(t.ToString())));
        }

        public List<Event> GetEventsByStory(int limit = 100, int offset = 0, bool getAll = false)
        {
            var parameters = new Dictionary<string, string> { { "id", Id.ToString() } };
            var array = Utilities.GetJArray("stories/{id}/events", limit, offset, getAll, parameters);
            return new List<Event>(array.Select(t => JsonConvert.DeserializeObject<Event>(t.ToString())));
        }

        #endregion

        #region tests

        [Fact]
        public void GetStoriesTest()
        {
            List<Story> stories = GetStories(getAll:false);
            foreach (Story story in stories)
            {
                Console.WriteLine("Title: {0}", story.Title);
            }
            Assert.True(stories.Count > 0, "Failed to get a list of stories.");
        }

        [Fact]
        public void GetStoryByIdTest()
        {
            Story story = GetStoryById(3);
            Assert.True(story.Title.Equals("Cover #3"), "failed to get the correct Story");
        }

        #endregion
    }
}
