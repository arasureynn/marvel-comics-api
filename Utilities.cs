using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using MarvelSharp.Models.Characters;
using MarvelSharp.Models.Creators;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace MarvelSharp
{
    public class Utilities
    {
        public static string PublicKey = ConfigurationManager.AppSettings["publicKey"];
        public static string PrivateKey = ConfigurationManager.AppSettings["privateKey"];

        #region JSON Helpers

        public static JObject GetRequestObject(string api, int id)
        {
            var client = new RestClient("http://gateway.marvel.com:80/v1/public/");
            var request = new RestRequest(api, Method.GET);
            var ts = DateTimeMillis();
            request.AddParameter("ts", ts);
            request.AddParameter("apikey", PublicKey);
            request.AddParameter("hash", GenerateMd5Hash(ts.ToString()));
            if (api.Contains("{id}"))
            {
                request.AddUrlSegment("id", id.ToString());
            }

            RestResponse response = client.Execute(request) as RestResponse;

            return JObject.Parse(response.Content);
        }

        public static JObject GetRequestObject(string api, Dictionary<string, string> additionalParameters)
        {
            var client = new RestClient("http://gateway.marvel.com:80/v1/public/");
            var request = new RestRequest(api, Method.GET);
            var ts = DateTimeMillis();
            request.AddParameter("ts", ts);
            request.AddParameter("apikey", PublicKey);
            request.AddParameter("hash", GenerateMd5Hash(ts.ToString()));

            if (additionalParameters.ContainsKey("id") && api.Contains("{id}"))
            {
                request.AddUrlSegment("id", additionalParameters["id"]);
                additionalParameters.Remove("id");
            }

            foreach (KeyValuePair<string, string> pair in additionalParameters)
            {
                request.AddParameter(pair.Key, pair.Value);
            }

            RestResponse response = client.Execute(request) as RestResponse;
            return JObject.Parse(response.Content);
        }

        public static JArray GetJArray(string request = null, int limit = 100, int offset = 0, bool getAll = true, Dictionary<string,string> parameters = null)
        {
            var array = new JArray();
            bool loadingResults = true;
            int total = 0;
            if (parameters == null) { parameters = new Dictionary<string, string>(); }
            parameters.Add("limit", limit.ToString());
            parameters.Add("offset", offset.ToString());
            while (loadingResults)
            {
                JObject s =
                    GetRequestObject(request, parameters);

                if (total == 0)
                {
                    total = Int32.Parse(s.GetValue("data").SelectToken("total").ToString());
                }

                foreach (JToken jToken in s.GetValue("data").SelectToken("results").ToObject<JArray>())
                {
                    array.Add(jToken);
                }

                offset += limit;
                loadingResults = getAll;

                // this is to make sure we dont go over, was not able to find a better way so far.
                if (total > 0 && total - offset < limit)
                {
                    limit = total - offset;
                    loadingResults = false;
                }
            }
            return array;
        }

        public static T GetJObject<T>(string request, int id)
        {
            JObject o = GetRequestObject(request, id);
            JToken chr = o.GetValue("data").SelectToken("results")[0];
            return JsonConvert.DeserializeObject<T>(chr.ToString());
        }

        public static T GetJObject<T>(string request, Dictionary<string,string> parameters)
        {
            JObject o = GetRequestObject(request, parameters);
            JToken chr = o.GetValue("data").SelectToken("results")[0];
            return JsonConvert.DeserializeObject<T>(chr.ToString());
        }

        #endregion

        #region MD5

        public static string GenerateMd5Hash(string ts)
        {
            string source = ts + PrivateKey + PublicKey;
            string hash;
            using (MD5 md5Hash = MD5.Create())
            {
                hash = GetMd5Hash(md5Hash, source);
            }
            return hash;
        }

        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            var sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        #endregion

        #region EnumHelpers

        public static String GetEnumDescription(Enum e)
        {
            Type type = e.GetType();
            MemberInfo[] memInfo = type.GetMember(e.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof (DescriptionAttribute), false);
                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute) attrs[0]).Description;
                }
            }
            return e.ToString();
        }

        #endregion

        #region Misc Helpers

        public static int DateTimeMillis()
        {
            TimeSpan t = DateTime.Now - new DateTime(1970, 1, 1);
            return (int)t.TotalSeconds;
        }

        #endregion
    }
}