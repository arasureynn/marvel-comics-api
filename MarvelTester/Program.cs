﻿using System;
using MarvelSharp.Models.Characters;
using MarvelSharp.Models.Comics;
using MarvelSharp.Models.Creators;
using MarvelSharp.Models.Events;
using MarvelSharp.Models.Series;
using MarvelSharp.Models.Stories;


namespace MarvelTester
{
    class Program
    {
        static void Main(string[] args)
        {

            var e = Event.EventGetById(314).GetCharactersByEvent();

            foreach (Character character in e)
            {
                Console.WriteLine(character.Name);
            }

            Console.WriteLine("Done");
            Console.Read();
        }
    }
}
